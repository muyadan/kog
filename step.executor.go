package kog

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"runtime/debug"
	"strings"
	"sync"
	"time"

	"github.com/digitalxero/slugify"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/pkg/stdcopy"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/pkg/errors"

	"gitlab.com/f5-pwe/common"
	"gitlab.com/f5-pwe/common/actions"
	"gitlab.com/f5-pwe/common/variables"
	"gitlab.com/f5-pwe/common/workflow"

	"gitlab.com/f5-pwe/kog/notifier"
)

const (
	defaultTimeout       = time.Hour * 24
	containerExitTimeout = time.Second * 30
)

func ExecuteStep(step workflow.WorkflowStep, job *pwe.Job, ctx variables.Context,
	logger log.Logger, notifierHooks *notifier.NotifyHooks, extraEnv map[string]string, volumes []string) actions.ActionResult {
	var (
		err           error
		action        actions.Action
		result        actions.ActionResult
		inputJsonData []byte
		containerId   string
		input, output types.HijackedResponse
		exitCode      int
	)

	defer func() {
		if r := recover(); r != nil {
			level.Error(logger).Log("msg", "Panic in ExecuteStep", "err", r, "traceback", debug.Stack())
		}
	}()

	actionLogger := log.With(logger, "action", step.ActionName)
	if action, err = job.Actions.Action(step.ActionName); err != nil {
		return NewStepResult(
			actions.ACTION_ABORT,
			fmt.Sprintf("Failed to get action \"%v\" for step \"%v\": %v", step.ActionName, step.Name, err.Error()),
		)
	}

	if extraEnv != nil {
		if action.Env == nil {
			action.Env = make(variables.Env, 0)
		}

		for key, value := range extraEnv {
			action.Env[key] = value
		}
	}

	// Create input context
	correlationId := job.ID.String()
	if _, ok := ctx["correlation_id"]; ok {
		correlationId = ctx["correlation_id"].(string)
	}
	runId := job.ExecutionID
	jobName := job.Name
	ctx["task_name"] = jobName
	ctx["run_id"] = runId
	ctx["correlation_id"] = correlationId
	ctx["step_name"] = step.Name
	ctx["action_name"] = action.Name
	containerName := slugify.IDify(fmt.Sprintf("%s-%s_%s-%s", jobName, step.Name, correlationId, runId))
	containerTimeout := defaultTimeout
	if step.TimeoutSeconds > 0 {
		containerTimeout = time.Duration(step.TimeoutSeconds) * time.Second
	}

	level.Debug(actionLogger).Log("msg", "Starting Container", "timeout", containerTimeout)

	if inputJsonData, err = json.Marshal(ctx); err != nil {
		return NewStepResult(
			actions.ACTION_ABORT,
			fmt.Sprintf("Invalid input context: %s", err.Error()),
		)
	}

	inputData := fmt.Sprintf("%s", inputJsonData)
	labels := make(map[string]string)
	labels["com.f5.pwe.job.type"] = "kog"
	labels["com.f5.pwe.job.name"] = jobName
	labels["com.f5.pwe.job.step.name"] = step.Name
	labels["com.f5.pwe.job.action.name"] = action.Name
	labels["com.f5.pwe.job.run_id"] = runId.String()
	labels["com.f5.pwe.job.context"] = inputData
	labels["com.f5.pwe.job.correlation_id"] = correlationId
	level.Debug(actionLogger).Log("labels", fmt.Sprintf("%+v", labels))
	if containerId, err = CreateContainer(
		containerName,
		action.Image,
		action.Tag,
		action.Command,
		action.Args,
		action.Env,
		volumes,
		labels,
		actionLogger,
	); err != nil {
		level.Error(actionLogger).Log("msg", "Error in create container", "err", err)
		return NewStepResult(actions.ACTION_ABORT, err.Error())
	}
	actionLogger = log.With(actionLogger, "container_id", containerId, "container", containerName)

	if input, err = AttachContainerStdin(containerId); err != nil {
		level.Error(actionLogger).Log("msg", "Error in attaching container stdin", "err", err)
		return NewStepResult(actions.ACTION_ABORT, err.Error())
	}
	defer input.Close()

	if output, err = AttachContainerOutput(containerId); err != nil {
		level.Error(actionLogger).Log("msg", "Error in attaching container output", "err", err)
		return NewStepResult(actions.ACTION_ABORT, err.Error())
	}
	defer output.Close()

	resultText := ""
	outR, outW := io.Pipe()
	outTee := ioutil.NopCloser(io.TeeReader(outR, os.Stdout))
	reader := bufio.NewReader(outTee)
	defer outW.Close()

	var done = &sync.WaitGroup{}
	done.Add(2)

	// fire notification hook just before starting the container step
	if err = notifierHooks.Fire(job); err != nil {
		level.Error(actionLogger).Log("msg", "Notify hook failed", "err", err)
	}

	if err = StartContainer(containerId); err != nil {
		level.Error(actionLogger).Log("msg", "Error in starting container", "err", err)
		return NewStepResult(actions.ACTION_ABORT, err.Error())
	}
	level.Info(actionLogger).Log("msg", "Step Running")

	// container output demuxing goroutine
	go func() {
		defer done.Done()
		if _, gerr := stdcopy.StdCopy(outW, os.Stderr, output.Reader); gerr != nil {
			level.Error(actionLogger).Log("msg", "Error in container output streaming", "err", gerr)
		}

		level.Debug(actionLogger).Log("msg", "Step standard streams copied")
	}()

	// container output scanning goroutine
	go func() {
		defer done.Done()
		for {
			line, err := reader.ReadString('\n')
			if err == io.EOF {
				break
			} else if err != nil {
				level.Error(actionLogger).Log("msg", "Error processing container output", "err", err)
				break
			}
			if strings.HasPrefix(line, "KOG:") {
				resultText = strings.TrimLeft(line, "KOG:")
				resultText = strings.TrimSpace(resultText)
				level.Debug(actionLogger).Log("msg", "Got result", "raw_result", resultText)
				break
			} else {
				level.Debug(actionLogger).Log("msg", line)
			}
		}
	}()

	if err = WriteContainerStdin(input.Conn, inputData); err != nil {
		level.Error(actionLogger).Log("msg", "Error writing container stdin", "err", err)
		return NewStepResult(actions.ACTION_ABORT, err.Error())
	}

	if timedout := waitTimeout(done, containerTimeout); timedout {
		if err = KillContainer(containerId); err != nil && !strings.Contains(err.Error(), "No such container") {
			level.Error(actionLogger).Log("msg", "Error killing container", "err", err)
		} else if err != nil {
			err = errors.Wrap(err, fmt.Sprintf("step \"%v\" timed out after %v", step.Name, containerTimeout))
			level.Error(actionLogger).Log("err", err)
			return NewStepResult(actions.ACTION_ABORT, err.Error())
		}
	}

	level.Debug(actionLogger).Log("msg", "Waiting for container to exit...")
	if exitCode, err = WaitForContainerExitWithTimeout(containerId, containerExitTimeout); err != nil {
		if kerr := KillContainer(containerId); err != nil && !strings.Contains(kerr.Error(), "No such container") {
			level.Error(actionLogger).Log("msg", "Error killing container", "err", kerr)
		}
		if strings.Contains(err.Error(), context.DeadlineExceeded.Error()) {
			msg := fmt.Sprintf("Container exit for step \"%v\" timed out after %v", step.Name, containerExitTimeout)
			return NewStepResult(actions.ACTION_ABORT, msg)
		}
		level.Error(actionLogger).Log("msg", "Error in container exit", "err", err)
	}

	if result, err = DecodeResults([]byte(resultText), actionLogger); err != nil {
		level.Error(actionLogger).Log("msg", "Could not parse result", "err", err, "result", resultText)
		return NewStepResult(actions.ACTION_ABORT, err.Error())
	}

	// Restore read-only keys
	result.Context["task_name"] = jobName
	result.Context["run_id"] = runId
	result.Context["correlation_id"] = correlationId
	result.Context["step_name"] = step.Name
	result.Context["action_name"] = action.Name
	result.ReturnCode = exitCode
	result.Name = step.Name

	if err = RemoveContainer(containerId); err != nil && !strings.Contains(err.Error(), "No such container") {
		level.Error(actionLogger).Log("msg", "Failed to remove container after successful execution", "err", err)
	} else {
		level.Debug(actionLogger).Log("msg", "Container Removed")
	}

	return result

}

func NewStepResult(result actions.ActionStatus, message string) actions.ActionResult {
	return actions.ActionResult{
		Result:       result,
		ReturnCode:   -1,
		Message:      message,
		NotifyStatus: "",
		Context:      make(variables.Context),
	}
}

func NextStep(step workflow.WorkflowStep, code actions.ActionStatus) string {
	switch code {
	case actions.ACTION_SUCCESS:
		return step.SuccessStep
	case actions.ACTION_FAIL:
		return step.FailStep
	case actions.ACTION_ABORT:
		return step.AbortStep
	}
	return ""
}
