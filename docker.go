/*
Copyright 2018 F5 Networks

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package kog

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strings"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/network"
	"github.com/docker/docker/client"
	"github.com/drone/envsubst"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
)

var (
	cli *client.Client
)

func init() {
	var err error
	cli, err = client.NewEnvClient()
	if err != nil {
		log.NewLogfmtLogger(os.Stderr).Log(level.Key(), level.ErrorValue(), "msg", "Error initializing Docker connection", "err", err)
		os.Exit(127)
	}
}

func PullImage(imageName string, tag string, logger log.Logger) error {
	imageTag := tag
	if tag == "" {
		imageTag = "latest"
	}
	pullLogger := level.Error(log.With(logger, "image", imageName, "tag", imageTag))
	opts := types.ImagePullOptions{}
	image := fmt.Sprintf("%s:%s", imageName, imageTag)
	output, err := cli.ImagePull(context.Background(), image, opts)
	if err != nil {
		return err
	}
	outdec := json.NewDecoder(output)
	type statusLine struct {
		Id       string `json:"id"`
		Status   string `json:"status"`
		Progress string `json:"progress"`
		Error    string `json:"error"`
	}
	for {
		line := statusLine{}
		if err := outdec.Decode(&line); err == io.EOF {
			break
		} else if err != nil {
			pullLogger.Log("err", err)
		} else {
			if line.Error != "" {
				return fmt.Errorf("%v", line.Error)
			}
		}
	}
	return nil
}

func CreateContainer(containerName string, imageName string, tag string, command string, args []string, env map[string]string, volumes []string, labels map[string]string, logger log.Logger) (string, error) {
	imageTag := tag
	if tag == "" {
		imageTag = "latest"
	}
	createLogger := level.Error(log.With(logger, "image", imageName, "tag", imageTag))

	image := fmt.Sprintf("%s:%s", imageName, imageTag)
	imageInfo, _, err := cli.ImageInspectWithRaw(context.Background(), image)
	if err != nil && client.IsErrImageNotFound(err) {
		if perr := PullImage(imageName, tag, logger); perr != nil {
			createLogger.Log("msg", "Failed to pull missing image", "err", perr)
			return "", perr
		}
	} else if err != nil {
		createLogger.Log("msg", "Error fetching image details", "err", err)
		return "", err
	}
	containerEnv := []string{}
	for k, v := range env {
		containerEnv = append(containerEnv, fmt.Sprintf("%s=%s", k, v))
		os.Setenv(k, v)
	}
	for k, v := range env {
		os.Setenv(k, v)
	}
	for _, imgEnv := range imageInfo.Config.Env {
		kv := strings.Split(imgEnv, "=")
		os.Setenv(kv[0], kv[1])
	}
	for idx, eEnv := range containerEnv {
		if eEnv, err := envsubst.EvalEnv(eEnv); err == nil {
			containerEnv[idx] = eEnv
		}
	}
	containerLabels := make(map[string]string)
	for k, v := range labels {
		containerLabels[k] = v
	}

	containerConf := container.Config{
		AttachStdin:  false,
		AttachStdout: false,
		AttachStderr: false,
		Tty:          false,
		OpenStdin:    true,
		StdinOnce:    true,
		Env:          containerEnv,
		Image:        image,
		StopSignal:   "SIGTERM",
		Labels:       containerLabels,
	}
	level.Debug(createLogger).Log("msg", "Container Config", "config", fmt.Sprintf("%+v", containerConf))
	if command != "" {
		cmd := []string{command}
		for _, arg := range args {
			cmd = append(cmd, arg)
		}
		containerConf.Cmd = cmd
		level.Debug(createLogger).Log("msg", "Container command", "cmd", cmd)
	} else {
		if len(args) > 0 {
			if err != nil {
				return "", err
			}
			cmd := imageInfo.Config.Cmd
			for _, arg := range args {
				cmd = append(cmd, arg)
			}
			containerConf.Cmd = cmd
			level.Debug(createLogger).Log("msg", "Container command", "cmd", cmd)
		}
	}

	hostConf := container.HostConfig{}
	if volumes != nil {
		for idx, vol := range volumes {
			if vol, err := envsubst.EvalEnv(vol); err == nil {
				volumes[idx] = vol
			}
		}
		hostConf.Binds = volumes
	}
	netConf := network.NetworkingConfig{}
	resp, err := cli.ContainerCreate(context.Background(), &containerConf, &hostConf, &netConf, containerName)
	if err != nil {
		return "", err
	}
	return resp.ID, nil
}

func AttachContainerStdin(containerId string) (types.HijackedResponse, error) {
	attachOpts := types.ContainerAttachOptions{
		Stream: true,
		Stdin:  true,
		Stdout: false,
		Stderr: false,
	}
	resp, err := cli.ContainerAttach(
		context.Background(),
		containerId,
		attachOpts,
	)
	if err != nil {
		return types.HijackedResponse{}, err
	}
	return resp, nil
}

func AttachContainerOutput(containerId string) (types.HijackedResponse, error) {
	attachOpts := types.ContainerAttachOptions{
		Stream: true,
		Stdin:  false,
		Stdout: true,
		Stderr: true,
	}
	resp, err := cli.ContainerAttach(
		context.Background(),
		containerId,
		attachOpts,
	)
	if err != nil {
		return types.HijackedResponse{}, err
	}
	return resp, nil
}

func StartContainer(containerId string) error {
	if err := cli.ContainerStart(context.Background(), containerId, types.ContainerStartOptions{}); err != nil {
		return err
	}
	return nil
}

func WaitForContainerExitWithTimeout(containerId string, timeout time.Duration) (statusCode int, err error) {
	resp, err := cli.ContainerInspect(context.Background(), containerId)
	if err != nil {
		return -1, err
	}

	state := resp.State
	if state != nil && state.Status == "exited" {
		return state.ExitCode, nil
	}

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	result, err := cli.ContainerWait(ctx, containerId)
	statusCode = 99
	if err != nil {
		return
	}
	statusCode = int(result)
	select {
	case <-ctx.Done():
		err = ctx.Err()
	}

	return
}

func GetContainerStdout(containerId string) (io.ReadCloser, error) {
	stdoutOpts := types.ContainerLogsOptions{
		ShowStdout: true,
		ShowStderr: false,
		Timestamps: false,
	}
	return cli.ContainerLogs(context.Background(), containerId, stdoutOpts)
}

func GetContainerStderr(containerId string) (io.ReadCloser, error) {
	stderrOpts := types.ContainerLogsOptions{
		ShowStdout: false,
		ShowStderr: true,
	}
	return cli.ContainerLogs(context.Background(), containerId, stderrOpts)
}

func RemoveContainer(containerId string) error {
	opts := types.ContainerRemoveOptions{
		Force:         false,
		RemoveVolumes: false,
		RemoveLinks:   false,
	}
	if err := cli.ContainerRemove(context.Background(), containerId, opts); err != nil {
		return err
	}
	return nil
}

func KillContainer(containerId string) error {
	if err := cli.ContainerKill(context.Background(), containerId, "SIGKILL"); err != nil {
		return err
	}
	return nil
}

func WriteContainerStdin(writer io.WriteCloser, data string) error {
	defer writer.Close()

	bufin := bufio.NewWriter(writer)
	_, err := bufin.WriteString(data)
	if err != nil {
		return err
	}

	err = bufin.Flush()
	if err != nil {
		return err
	}

	return nil
}
