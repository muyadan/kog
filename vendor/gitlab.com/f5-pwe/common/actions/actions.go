/*
Copyright 2018 F5 Networks

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package actions

import (
	"encoding/json"
	"fmt"

	"gitlab.com/f5-pwe/common/variables"
)

type Actions map[string]Action

type tmpA Actions

func (a *Actions) UnmarshalJSON(b []byte) (err error) {
	var newA tmpA
	if err = json.Unmarshal(b, &newA); err == nil {
		for name, action := range newA {
			action.Name = name
			newA[name] = action
		}
		*a = Actions(newA)
	}
	return
}

func (a Actions) Action(name string) (Action, error) {
	if action, ok := a[name]; ok {
		return action, nil
	}

	return Action{}, fmt.Errorf("Action %s does not exist", name)
}

type Action struct {
	Name    string         `json:"-" yaml:"-"`
	Image   string         `json:"image" yaml:"image"`
	Tag     string         `json:"version" yaml:"version"`
	Command string         `json:"command,omitempty" yaml:"command,omitempty"`
	Args    variables.Args `json:"args,omitempty" yaml:"args,omitempty"`
	Env     variables.Env  `json:"env,omitempty" yaml:"env,omitempty"`
}

type ActionStatus int

var actionstatusStrings = [...]string{
	"invalid",
	"success",
	"fail",
	"failure",
	"abort",
}

const (
	ACTION_INVALID ActionStatus = iota
	ACTION_SUCCESS
	ACTION_FAIL
	ACTION_FAILURE
	ACTION_ABORT
)

func (as ActionStatus) String() string {
	return actionstatusStrings[as]
}

func (as ActionStatus) MarshalJSON() ([]byte, error) {
	return json.Marshal(as.String())
}

func (as *ActionStatus) UnmarshalJSON(b []byte) (err error) {
	var val string
	err = json.Unmarshal(b, &val)
	if err != nil {
		*as = ACTION_INVALID
		return
	}

	if actionstatusStrings[ACTION_SUCCESS] == val {
		*as = ACTION_SUCCESS
	} else if actionstatusStrings[ACTION_FAIL] == val {
		*as = ACTION_FAIL
	} else if actionstatusStrings[ACTION_FAILURE] == val {
		*as = ACTION_FAIL
	} else if actionstatusStrings[ACTION_ABORT] == val {
		*as = ACTION_ABORT
	} else {
		*as = ACTION_INVALID
	}

	return
}

type ActionResult struct {
	Name         string            `json:"step_name" yaml:"step_name"`
	Result       ActionStatus      `json:"result" yaml:"result"`
	ReturnCode   int               `json:"rc" yaml:"rc"`
	Message      string            `json:"message" yaml:"message"`
	NotifyStatus string            `json:"notify_status,omitempty" yaml:"notify_status,omitempty"`
	Duration     float64           `json:"duration_seconds" yaml:"duration_seconds"`
	Context      variables.Context `json:"context" yaml:"context"`
}

type tmpAR ActionResult

func (a *ActionResult) UnmarshalJSON(b []byte) (err error) {
	var newAR tmpAR
	if err = json.Unmarshal(b, &newAR); err == nil {
		*a = ActionResult(newAR)
	}
	return
}
