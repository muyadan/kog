/*
Copyright 2018 F5 Networks

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package workflow

import (
	"encoding/json"
	"fmt"
)

type Workflows map[string]Workflow

func (w Workflows) Workflow(name string) (Workflow, error) {
	if workflow, ok := w[name]; ok {
		return workflow, nil
	}
	return Workflow{}, fmt.Errorf("Workflow %s does not exist", name)
}

func (w Workflows) Exists(name string) bool {
	_, ok := w[name]
	return ok
}

type Workflow map[string]WorkflowStep
type tmpW Workflow

func (w *Workflow) UnmarshalJSON(b []byte) (err error) {
	var newW tmpW
	err = json.Unmarshal(b, &newW)
	if err != nil {
		return
	}
	tmp := Workflow(newW)
	err = tmp.Validate()
	*w = Workflow(tmp)
	return
}

func (w Workflow) Validate() error {
	for name, step := range w {
		step.Name = name
		w[name] = step
	}
	if _, ok := w["start"]; !ok {
		return fmt.Errorf("Invalid Workflow, missing start step")
	}
	for k, step := range w {
		if step.SuccessStep != "" {
			if _, ok := w[step.SuccessStep]; !ok {
				return fmt.Errorf("invalid workflow step \"%s\": \"success\" step must be \"\" or the name of a step in the workflow",
					k)
			}
		}
		if step.FailStep != "" {
			if _, ok := w[step.FailStep]; !ok {
				return fmt.Errorf("invalid workflow step \"%s\": \"fail\" step must be \"\" or the name of a step in the workflow",
					k)
			}
		}
		if step.AbortStep != "" {
			if _, ok := w[step.AbortStep]; !ok {
				return fmt.Errorf("invalid workflow step \"%s\": \"abort\" step must be \"\" or the name of a step in the workflow",
					k)
			}
		}
	}

	return nil
}

func (w Workflow) Step(name string) (WorkflowStep, error) {
	if step, ok := w[name]; ok {
		return step, nil
	}
	return FailedStep, fmt.Errorf("Step %s does not exist", name)
}

type WorkflowStep struct {
	Name           string  `json:"name" yaml:"name"`
	ActionName     string  `json:"action" yaml:"action"`
	SuccessStep    string  `json:"success,omitempty" yaml:"success,omitempty"`
	FailStep       string  `json:"fail,omitempty" yaml:"fail,omitempty"`
	AbortStep      string  `json:"abort,omitempty" yaml:"abort,omitempty"`
	TimeoutStep    string  `json:"timeout,omitempty" yaml:"timeout,omitempty"`
	TimeoutSeconds float64 `json:"timeout_seconds,omitempty" yaml:"timeout_seconds,omitempty"`
}

var FailedStep = WorkflowStep{}
