package main

import (
	"os"
	"path"
	"strings"

	"github.com/spf13/viper"
)

func setupConfig() (config *viper.Viper) {
	config = viper.New()
	config.SetEnvPrefix(pkgName)
	config.AutomaticEnv()
	config.SetEnvKeyReplacer(strings.NewReplacer(".", "_", "-", ""))
	config.SetDefault("debug", false)
	config.SetDefault("config.type", "yaml")
	config.SetDefault("config.name", pkgName)
	config.SetConfigType(config.GetString("config.type"))
	config.SetConfigName(config.GetString("config.name"))
	config.SetDefault("dirs.bin", os.Getenv("bin_dir"))
	config.SetDefault("dirs.etc", os.Getenv("etc_dir"))
	config.SetDefault("dirs.data", os.Getenv("data_dir"))

	config.SetDefault("user", os.Getenv("USER"))
	config.SetDefault("config.path", path.Join(config.GetString("dirs.etc"), "pwe"))
	config.AddConfigPath(config.GetString("config.path"))
	config.ReadInConfig()
	config.Set("app.name", pkgName)
	config.Set("app.version", version)
	config.Set("app.branch", repoBranch)

	return
}
