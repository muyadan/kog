package main

import (
	"os"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/f5-pwe/kog/notifier"
)

var (
	pkgName    = "kog"
	version    = "0.0.1"
	repoBranch = "develop"
	cmdRoot    = &cobra.Command{
		Use:   pkgName,
		Short: "PWE Workflow Executor",
	}
	debug         = false
	baseLogger    log.Logger
	notifierHooks *notifier.NotifyHooks
)

func main() {
	//Server Config domain, this should NEVER leave the main package
	config := setupConfig()

	cmdRoot.PersistentFlags().BoolVarP(
		&debug,
		"debug",
		"",
		debug,
		``)
	viper.BindPFlag("debug", cmdRoot.PersistentFlags().Lookup("debug"))

	cmdRoot.PersistentFlags().StringSliceP(
		"notifier",
		"n",
		[]string{},
		`add notifier hook (url to post to)`)
	viper.BindPFlag("notifiers", cmdRoot.PersistentFlags().Lookup("notifier"))

	cmdRoot.PersistentFlags().StringP(
		"log-formatter",
		"F",
		"logfmt",
		`Select the formatter for log records ("logfmt" or "json")`)
	viper.BindPFlag("logger.formatter", cmdRoot.PersistentFlags().Lookup("log-formatter"))

	cmdRoot.PersistentFlags().StringP(
		"log-level",
		"L",
		"info",
		`Set the logging level ("debug", "info", "warn", "error", "none")`)
	viper.BindPFlag("logger.level", cmdRoot.PersistentFlags().Lookup("log-level"))

	cmdRoot.PersistentFlags().StringP(
		"remote-logger",
		"R",
		"",
		`Remote logger ("syslog")`,
	)
	viper.BindPFlag("logger.remote", cmdRoot.PersistentFlags().Lookup("remote-logger"))

	cmdRoot.PersistentFlags().StringP(
		"remote-logger-proto",
		"",
		"udp",
		`syslog protocol`,
	)
	viper.BindPFlag("logger.proto", cmdRoot.PersistentFlags().Lookup("remote-logger-proto"))

	cmdRoot.PersistentFlags().StringP(
		"remote-logger-addr",
		"",
		"127.0.0.1:514",
		`syslog addr`,
	)
	viper.BindPFlag("logger.addr", cmdRoot.PersistentFlags().Lookup("remote-logger-addr"))

	cmdRoot.PersistentPreRun = func(c *cobra.Command, args []string) {
		baseLogger = setupLogger(config)
		notifierHooks = setupNotifyHooks(config)

		level.Debug(baseLogger).Log("msg", "kog PersistentPreRun called")
	}

	if err := startCLI(config); err != nil {
		os.Exit(127)
	}
}

func startCLI(config *viper.Viper) (err error) {
	var cmds []*cobra.Command
	cmds = append(cmds, setupRunCmd(config))
	cmdRoot.AddCommand(cmds...)

	// Run!
	return cmdRoot.Execute()
}
