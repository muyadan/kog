package main

import (
	"strings"

	"github.com/go-kit/kit/log/level"
	"github.com/spf13/viper"

	"gitlab.com/f5-pwe/kog/notifier"
)

func setupNotifyHooks(cfg *viper.Viper) (notifiers *notifier.NotifyHooks) {
	notifiers = &notifier.NotifyHooks{}

	for _, hook := range cfg.GetStringSlice("notifier") {
		if strings.HasPrefix(hook, "http") {
			notifiers.Add(notifier.NewHTTPNotifyHook(hook, baseLogger))
			level.Debug(baseLogger).Log("msg", "Added http notify hook", "notifier", hook)
		} else if hook == "console" {
			notifiers.Add(notifier.NewConsoleHook(baseLogger))
			level.Debug(baseLogger).Log("msg", "Added console notify hook")
		} else {
			level.Error(baseLogger).Log("err", "Unknown Notifier Type", "notifier", hook)
		}
	}

	return
}
