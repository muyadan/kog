# Kog - docker workflow engine
A cross-platform CLI to execute a PWE job using docker containers.

## Dev Environment Setup
1. Install Go language on your system. You can use built-in package manager system and look for `go` or `golang`.
For more advanced instruction, see [official documentation](https://golang.org/doc/install).
2. Create the folder for the project, this is important since Go expects packages to be under 
```
mkdir -p ~/go/src/gitlab.com/f5-pwe
cd ~/go/src/gitlab.com/f5-pwe
```
3. Clone this project
```
git clone git@gitlab.com:f5-pwe/kog.git
```
4. Compile and install kog into current directory
```
cd kog
make build
```
5. Verify you can run kog
```
./bin/kog version
```

## Execute Jobs
Use `run` command to execute jobs in kog.

### Docker
Kog executes jobs using docker. There are options for configuring docker:
- Set in `DOCKER_HOST` environment variable to a valid docker host, for example `tcp://localhost:2375`

### Workflows
By default, kog expects workflow input in stdin; it's also possible to provide input from file or url using `-f` flag.


There are few workflows checked in under `data` folder. The simplest workflow to execute is `data/test/success.json`, which executes echo command in alpine container.
```
./bin/kog run -f data/test/success.yaml
```

## Environment
The easiest way to get started using different environments, such as dev, silver and gold, is to use `env` flag.
These settings can be overridden by command line flags.

## Logging
By default, Kog logs to console in [logfmt](https://github.com/go-logfmt/logfmt) format. It is possible to switch format to JSON by settings `--log-formatter` flag to `json`.
The log settings will also be propagated to all spawned containers using passed context.

## Remote Logging
To enable log aggregation, it is useful to set `--remote-logger` flag to either `syslog` or `amqp`.
Enabling remote logger does not disable local logs, but adds a handler to ship logs to a remote location.

## Status Notification
To enable step result notifications, set `--notifier` flag to either `amqp`, `console`, or `http`
This will enable sending a notification on every step start to keep track of a job progress.

## Return Codes
By default the `kog run` command will use the job status to map to return codes, use `--use-job-rc=false` to always return 0 (except for issues with the kog command itself)

* 127 - error with the kog command itself
* [Job Status Return Codes](https://gitlab.com/f5-pwe/common/blob/master/job.go#L176)

## Usage
```
...
```