package kog

import (
	"bufio"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"

	"github.com/ghodss/yaml"
	"gitlab.com/f5-pwe/common"
	"gitlab.com/f5-pwe/common/variables"
)

func FileOrURLReader(taskFilename string, logger log.Logger) (reader io.Reader, err error) {
	var body []byte
	if taskFilename != "" && !strings.HasPrefix(taskFilename, "http") {
		level.Info(logger).Log("msg", "Reading", "file", taskFilename)
		body, err = ioutil.ReadFile(taskFilename)
		reader = strings.NewReader(string(body))
	} else if taskFilename != "" && strings.HasPrefix(taskFilename, "http") {
		level.Info(logger).Log("msg", "Reading", "url", taskFilename)
		client := &http.Client{Timeout: time.Second * 60}
		var resp *http.Response
		resp, err = client.Get(taskFilename)
		if err != nil {
			return
		}
		defer resp.Body.Close()
		if err = checkResponse(resp); err != nil {
			return
		}
		body, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}
		reader = strings.NewReader(string(body))
	} else {
		// stdin
		reader = bufio.NewReader(os.Stdin)
	}
	return
}

func ParseContext(data []byte) (ctx variables.Context, err error) {
	if err = json.Unmarshal(data, &ctx); err != nil {
		if err = yaml.Unmarshal(data, &ctx); err != nil {
			return nil, err
		}
	}

	return ctx, nil
}

func ParseJob(data []byte) (job *pwe.Job, err error) {
	if err = json.Unmarshal(data, &job); err != nil {
		if err = yaml.Unmarshal(data, &job); err != nil {
			return nil, err
		}
	}
	// Get the task id
	if job.ID == "" {
		job.ID = pwe.NewRequestID()
	}
	if job.ExecutionID == "" {
		job.ExecutionID = pwe.NewRequestID()
	}

	return job, nil
}
