/*
Copyright 2018 F5 Networks

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package notifier

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"path"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"gitlab.com/f5-pwe/common"
)

type httpNotifyHook struct {
	BaseURL string
	Client  *http.Client
	logger  log.Logger
}

func NewHTTPNotifyHook(baseURL string, logger log.Logger) NotifyHook {
	client := &http.Client{Timeout: time.Second * 60}
	hook := httpNotifyHook{
		BaseURL: baseURL,
		Client:  client,
	}
	hook.logger = log.With(logger, "component", "http-notifier")
	return &hook
}

func (h *httpNotifyHook) Fire(job *pwe.Job) error {
	body, err := json.Marshal(job)
	if err != nil {
		return err
	}

	u, err := h.buildURL("job", job.ID.String())
	if err != nil {
		return err
	}
	level.Debug(h.logger).Log("msg", "HTTP hook request", "url", u, "body", string(body))

	resp, err := h.Client.Post(u, "application/json", bytes.NewBuffer(body))
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		level.Debug(h.logger).Log("msg", "HTTP hook response", "url", u, "status", resp.Status)
		return err
	}
	level.Debug(h.logger).Log("msg", "HTTP hook response", "url", u, "status", resp.Status, "body", string(respBody))

	return nil
}

func (h *httpNotifyHook) buildURL(parts ...string) (string, error) {
	u, err := url.Parse(h.BaseURL)
	if err != nil {
		return "", err
	}
	u.Path = path.Join(append([]string{u.Path}, parts...)...)
	return u.String(), err
}
