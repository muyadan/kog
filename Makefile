ifndef GOPATH
$(warning You need to set up a GOPATH. Run "go help gopath".)
endif


PKG_NAME?=kog
DOCKER_HOST?=tcp://localhost:2375
version:=0.0.19
run_id:=$(shell date -u '+%Y%m%d-%H%M%S')

git:=$(strip $(shell which git 2> /dev/null))
go:=$(strip $(shell which go 2> /dev/null))
ifneq ($(go),)
	packages:=$(shell $(go) list ./... | grep -v /vendor/)
endif

ifeq ($(release),)
ifeq ($(git),)
	release:=$(strip $(shell date +%s))
else
	release:=$(strip $(shell $(git) log -1 --pretty=format:%ct))
endif
endif

GOOS?=linux
GOARCH?=amd64
GO_PROJECT_DIR?=$(strip $(shell pwd))


.PHONY: build run test vendor lin32 lin64 win32 win64 linux windows dar64 darwin docker all push hello-world

all: build

clean:
	rm -rf bin

build:
	GOOS=$(GOOS) GOARCH=$(GOARCH) CGO_ENABLED=1 $(go) build -v -tags netgo -ldflags '-extldflags "-static" -X main.pkgName=$(PKG_NAME) -X main.version=$(version)-$(release_tag) -X main.repoBranch=$(repoBranch)' -o ./bin/$(GOOS)/$(GOARCH)/$(PKG_NAME)$(BIN_EXTENSION) $(GO_PROJECT_DIR)/cmd/$(PKG_NAME)/*.go

lin32:
	GOOS=linux GOARCH=386 $(MAKE) build

lin64:
	GOOS=linux GOARCH=amd64 $(MAKE) build

linux: lin32 lin64

win32:
	GOOS=windows GOARCH=386 BIN_EXTENSION=.exe $(MAKE) build

win64:
	GOOS=windows GOARCH=amd64 BIN_EXTENSION=.exe $(MAKE) build

windows: win32 win64

dar64:
	GOOS=darwin GOARCH=amd64 $(MAKE) build

darwin: dar64

run: build
	./bin/$(GOOS)/$(GOARCH)/$(PKG_NAME) version

format:
	$(go) fmt $(packages)

test: format
	$(go) vet $(packages)
	DOCKER_HOST=$(DOCKER_HOST) $(go) test -v -race $(packages)

vendor:
	dep ensure
